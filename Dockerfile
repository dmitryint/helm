FROM alpine/helm:3.10.1

RUN apk -U add jq yq curl git bash gcompat \
    && cd /tmp \
    && curl -sSL  https://github.com/alekc-forks/helm-mirror/releases/download/v0.3.2/helm-mirror_0.3.2_linux_amd64.tar.gz -O \
    && tar -xf helm-mirror_0.3.2_linux_amd64.tar.gz \
    && mv helm-mirror /usr/local/bin/ \
    && rm -rf helm-mirror_0.3.2_linux_amd64.tar.gz

SHELL ["/bin/bash", "-c"]
